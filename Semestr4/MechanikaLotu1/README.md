# Mechanika Lotu 1

Nie mieliśmy kolokwium.

Miałem samolot **odrzutowy**. Silnik do Jak-23 znajduje się w hali w ITC, polecam spojrzeć bo są tam użyteczne informacje które inaczej ciężko znaleźć. Oczywiście znalazłem ten silnik w hali dopiero po zrobieniu projektu 5. o((>ω< ))o

Warto sobie jakiś workflow wyrobić do projektów z Mechaniki Lotu, bo jest ich łącznie 11 do zrobienia.
Ja robiłem obliczenia w *Matlab*-ie (równie dobrze można w Pythonie) i robiłem raporty w *LaTeX*-ie, do którego eksportowałem zmienne, tabele i grafiki z *MATLAB*-a. Trochę to było męczące by ogarnąć na początku ale jak już działało to nawet przyjemnie się te projekty robiło.\
Zarówno *Matlab*-a jak i *LaTeX*-a pisałem pod koniec w VS-code, co polecam.\
Zdecydowanie polecam także ogarnięcie GirHub Copilot, sprawia że pisanie projektów przestaje być irytujące.
\
Wykresy zapisywane w formacie wektorowym (.eps) ładnie wyglądają.